# README - MockAPI #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Mock an API

### How do I get set up? ###

* Dependencies
    *  Docker Engine

* Repo owner or admin
    * lcjr86

### How to use it? ###

* Clone the repo
* Navegate to the project folder
* Build the image (if you don't have yet)
    * On the terminal, run ```docker build --tag mock-api .```
* Run the image
    * On the terminal, run ```docker run -d -p 5000:5000 mock-api```
* Get the results (```GET``` example)
    * On the browser, run ```http://localhost:5000/hello```
    * You should get a .json in response
        ```json
        {"data":"Hello World"}
        ```
* Stop to docker
    * On the terminal, run ```docker ps``` and get the ```CONTAINER ID```from ```IMAGE = mock-api````
    * Then, run ```docker stop <CONTAINER ID> ```, replacing the ```CONTAINER ID``` value by the value that you get on the previous step.

### Add more methods to test ###
* Go to the ```app.py```file and create a new 'block' of code (between ```app = Flask(__name__)``` and ```if __name__ == '__main__':```).

    * ```GET```
        ```python
        @app.route('/<REQUEST_NAME>', methods=['GET'])
        def <REQUEST_NAME>():
            if(request.method == 'GET'):
                data = <DATA_THAT_YOU_WANT_TO_GET>
                return jsonify(data)
        ```

        ```Example:```
        ```python
        @app.route('/hello', methods=['GET'])
        def helloworld():
            if(request.method == 'GET'):
                data = {"data": "Hello World"}
                return jsonify(data)
        ```

    * ```POST```
        ```python
        @app.route('/<REQUEST NAME>', methods = ['POST']) 
        def new_user():
            if(request.method == 'POST'):
                data = request.get_json() #gets JSON data
                # put here what do you want to do with the 'data'
                res = {‘status’: ‘ok’}
                return jsonify(res)
        ```
        ```Example:```
        ```python
        @app.route('/users', methods = ['POST']) 
        def new_user():
            if(request.method == 'POST'):
                user_data = request.get_json() #gets JSON data
                res = {‘status’: ‘ok’}
                return jsonify(res)
        ```        

### References ###

* Docker: https://docs.docker.com/engine/reference/builder/
* Docker + flask: https://www.freecodecamp.org/news/how-to-dockerize-a-flask-app/
* Flask GET example: https://www.geeksforgeeks.org/how-to-write-a-simple-flask-api-for-hello-world/
* Flask POST example: https://tutorials.technology/tutorials/flask-example-with-POST-API-endpoint.html