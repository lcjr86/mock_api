from flask import Flask, jsonify, request
  
app = Flask(__name__) 
  
@app.route('/hello', methods=['GET'])
def helloworld():
    if(request.method == 'GET'):
        data = {"data": "Hello World"}
        return jsonify(data)


@app.route('/users', methods = ['POST']) 
def new_user():
    if(request.method == 'POST'):
        user_data = request.get_json() # 
        # add here the code to create the user
        res = {‘status’: ‘ok’}
        return jsonify(res)


if __name__ == '__main__':
    app.run(debug=True)